﻿<%@ Page Title="Edit Pages" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Pages.aspx.cs" Inherits="Management_Pages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataPages" EmptyDataText="There are no pages yet." Width="100%">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" >
            <ItemStyle VerticalAlign="Top" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>' TextMode="MultiLine" Rows="5" Width="250px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="300px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Owner" SortExpression="Owner">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataUsers" SelectedValue='<%# Bind("Owner") %>' DataTextField="UserName" DataValueField="UserName"></asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Owner") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:CommandField ButtonType="Button" HeaderText="Action" ShowDeleteButton="True" ShowEditButton="True" >
            <ItemStyle VerticalAlign="Top" />
            </asp:CommandField>
        </Columns>
</asp:GridView>
    <br />
<asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="SqlDataPages" DefaultMode="Insert" Width="100%" HeaderText="Create a Page">
    <Fields>
        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
        <asp:TemplateField HeaderText="Name" SortExpression="Name">
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox><asp:RequiredFieldValidator ErrorMessage="You must supply a page name." ControlToValidate="TextBox2" runat="server" CssClass="ErrorMessage" ValidationGroup="DetailsView1" />
            </InsertItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" SortExpression="Description">
            <InsertItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>' TextMode="MultiLine" Width="500px" Rows="5"></asp:TextBox>
            </InsertItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Owner" SortExpression="Owner">
            <InsertItemTemplate>
                <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("Owner") %>' DataSourceID="SqlDataUsers" DataTextField="UserName" DataValueField="UserName"></asp:DropDownList>
            </InsertItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ButtonType="Button" ShowInsertButton="True" CancelText="Clear" ValidationGroup="DetailsView1" />
    </Fields>
    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
</asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataUsers" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" SelectCommand="SELECT [UserId], [UserName] FROM [Users]"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataPages" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" DeleteCommand="DELETE FROM [Page] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Page] ([Name], [Description], [Owner]) VALUES (@Name, @Description, @Owner)" SelectCommand="SELECT * FROM [Page]" UpdateCommand="UPDATE [Page] SET [Name] = @Name, [Description] = @Description, [Owner] = @Owner WHERE [Id] = @Id" CacheDuration="600">
    <DeleteParameters>
        <asp:Parameter Name="Id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="Description" Type="String" />
        <asp:Parameter Name="Owner" Type="String" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="Description" Type="String" />
        <asp:Parameter Name="Owner" Type="String" />
        <asp:Parameter Name="Id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
</asp:Content>

