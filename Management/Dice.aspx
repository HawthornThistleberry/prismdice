﻿<%@ Page Title="Edit Dice" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Dice.aspx.cs" Inherits="Management_Dice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataDie" EmptyDataText="There are no dice defined yet.">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="Die.aspx?Id={0}" DataTextField="Name" HeaderText="Name" />
            <asp:TemplateField HeaderText="Page" SortExpression="Page">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# GetPageName(Eval("Page")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PagePos" HeaderText="Position" SortExpression="PagePos" />
            <asp:TemplateField HeaderText="Published" SortExpression="Published">
                <ItemTemplate>
                    <asp:Label ID="PublishedLabel" runat="server" Text='<%# GetBooleanText(Eval("Published")) %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
        </Columns>
    </asp:GridView>
    <a href="Die.aspx">Insert New Die</a>
    <asp:SqlDataSource ID="SqlDataDie" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" DeleteCommand="DELETE FROM [Die] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Die] ([Id], [Name], [Page], [NumDice], [NumSides], [DieAdder], [Description], [Published], [DieType], [DieParameter], [TotalAdder], [TotalMultiplier], [SortRolls], [PagePos]) VALUES (@Id, @Name, @Page, @NumDice, @NumSides, @DieAdder, @Description, @Published, @DieType, @DieParameter, @TotalAdder, @TotalMultiplier, @SortRolls, @PagePos)" SelectCommand="SELECT * FROM [Die]" UpdateCommand="UPDATE [Die] SET [Name] = @Name, [Page] = @Page, [NumDice] = @NumDice, [NumSides] = @NumSides, [DieAdder] = @DieAdder, [Description] = @Description, [Published] = @Published, [DieType] = @DieType, [DieParameter] = @DieParameter, [TotalAdder] = @TotalAdder, [TotalMultiplier] = @TotalMultiplier, [SortRolls] = @SortRolls, [PagePos] = @PagePos WHERE [Id] = @Id" CacheDuration="600">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Page" Type="Int32" />
            <asp:Parameter Name="NumDice" Type="Int32" />
            <asp:Parameter Name="NumSides" Type="Int32" />
            <asp:Parameter Name="DieAdder" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Published" Type="Boolean" />
            <asp:Parameter Name="DieType" Type="Int16" />
            <asp:Parameter Name="DieParameter" Type="Int32" />
            <asp:Parameter Name="TotalAdder" Type="Int32" />
            <asp:Parameter Name="TotalMultiplier" Type="Int32" />
            <asp:Parameter Name="SortRolls" Type="Int16" />
            <asp:Parameter Name="PagePos" Type="Int16" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Page" Type="Int32" />
            <asp:Parameter Name="NumDice" Type="Int32" />
            <asp:Parameter Name="NumSides" Type="Int32" />
            <asp:Parameter Name="DieAdder" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Published" Type="Boolean" />
            <asp:Parameter Name="DieType" Type="Int16" />
            <asp:Parameter Name="DieParameter" Type="Int32" />
            <asp:Parameter Name="TotalAdder" Type="Int32" />
            <asp:Parameter Name="TotalMultiplier" Type="Int32" />
            <asp:Parameter Name="SortRolls" Type="Int16" />
            <asp:Parameter Name="PagePos" Type="Int16" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataPages" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" SelectCommand="SELECT [Id], RTRIM([Name]) + ' (' + RTRIM([Owner]) + ')' AS PageName FROM Page" CacheDuration="600"></asp:SqlDataSource>
</asp:Content>

