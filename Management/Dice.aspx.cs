﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PrismDiceModel;

public partial class Management_Dice : BasePage
{
    protected string GetBooleanText(object booleanValue)
    {
        bool published = (bool)booleanValue;
        if (published)
        {
            return "Yes";
        }
        else
        {
            return "No";
        }
    }

    protected string GetPageName(object pageID)
    {
        int pageNum = (int)pageID;
        using (PrismDiceEntities myEntities = new PrismDiceEntities()) {
            var thisPage = (from r in myEntities.Pages
                            where r.Id == pageNum
                            select r).Single();
            var thisOwner = (from r in myEntities.Users
                             where r.UserName == thisPage.Owner
                             select r).Single();
            return thisPage.Name + " (" + thisOwner.UserName + ")";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}