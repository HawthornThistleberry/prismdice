﻿<%@ Page Title="Edit A Die" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Die.aspx.cs" Inherits="Management_Die" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="SqlDataDie" DefaultMode="Insert" OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemCommand="DetailsView1_ItemCommand">
        <Fields>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox><asp:RequiredFieldValidator ErrorMessage="Dice must be labelled." ControlToValidate="TextBox2" runat="server" CssClass="ErrorMessage" /><br />
                    <asp:Label ID="Label2" runat="server" Text="The name is shown on the dice button." CssClass="DieExplanation"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox><asp:RequiredFieldValidator ErrorMessage="Dice must be labelled." ControlToValidate="TextBox2" runat="server" CssClass="ErrorMessage" /><br />
                    <asp:Label ID="Label3" runat="server" Text="The name is shown on the dice button." CssClass="DieExplanation"></asp:Label>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Page" SortExpression="Page">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList3" runat="server" SelectedValue='<%# Bind("Page") %>' DataSourceID="SqlSourcePage" DataTextField="Text" DataValueField="Id"></asp:DropDownList>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList3" runat="server" SelectedValue='<%# Bind("Page") %>' DataSourceID="SqlSourcePage" DataTextField="Text" DataValueField="Id"></asp:DropDownList>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PagePos" SortExpression="PagePos">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PagePos") %>' TextMode="Number" min="1" max="8"></asp:TextBox><asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Position must be in the range 1-8." ControlToValidate="TextBox3" CssClass="ErrorMessage" MaximumValue="8" MinimumValue="1"></asp:RangeValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PagePos") %>' TextMode="Number" min="1" max="8"></asp:TextBox><asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Position must be in the range 1-8." ControlToValidate="TextBox3" CssClass="ErrorMessage" MaximumValue="8" MinimumValue="1"></asp:RangeValidator>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Number of Dice" SortExpression="NumDice">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("NumDice") %>' TextMode="Number" min="1" max="999" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="You can have from 1 to 999 dice." ControlToValidate="TextBox4" CssClass="ErrorMessage" MaximumValue="999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("NumDice") %>' TextMode="Number" min="1" max="999" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="You can have from 1 to 999 dice." ControlToValidate="TextBox4" CssClass="ErrorMessage" MaximumValue="999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Number of Sides" SortExpression="NumSides">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("NumSides") %>' TextMode="Number" min="2" max="10000" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator5" runat="server" ErrorMessage="Dice can have from 2 to 10000 sides." ControlToValidate="TextBox5" CssClass="ErrorMessage" MinimumValue="2" MaximumValue="10000" Type="Integer"></asp:RangeValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("NumSides") %>' TextMode="Number" min="2" max="10000" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator6" runat="server" ErrorMessage="Dice can have from 2 to 10000 sides." ControlToValidate="TextBox5" CssClass="ErrorMessage" MinimumValue="2" MaximumValue="10000" Type="Integer"></asp:RangeValidator>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Die Adder" SortExpression="DieAdder">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("DieAdder") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="Label4" runat="server" Text="Add this value to every die rolled." CssClass="DieExplanation"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("DieAdder") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="Label4" runat="server" Text="Add this value to every die rolled." CssClass="DieExplanation"></asp:Label>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Die Type" SortExpression="DieType">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="DieTypes" DataTextField="dicename" DataValueField="diceval" SelectedValue='<%# Bind("DieType") %>'></asp:DropDownList><br />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="DieTypes" DataTextField="dicename" DataValueField="diceval" SelectedValue='<%# Bind("DieType") %>'></asp:DropDownList><br />
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Parameter" SortExpression="DieParameter">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DieParameter") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="DieParamDesc" runat="server" Text="" CssClass="DieExplanation"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("DieParameter") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="DieParamDesc" runat="server" Text="" CssClass="DieExplanation"></asp:Label>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Adder" SortExpression="TotalAdder">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("TotalAdder") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="Label5" runat="server" Text="Add this to the roll total." CssClass="DieExplanation"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("TotalAdder") %>' TextMode="Number" Width="100px"></asp:TextBox><br />
                    <asp:Label ID="Label5" runat="server" Text="Add this to the roll total." CssClass="DieExplanation"></asp:Label>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total Multiplier" SortExpression="TotalMultiplier">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("TotalMultiplier") %>' TextMode="Number" min="1" max="99" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="Multipliers must range between 1 and 99." ControlToValidate="TextBox6" CssClass="ErrorMessage" MinimumValue="1" MaximumValue="99" Type="Integer"></asp:RangeValidator><br />
                    <asp:Label ID="Label6" runat="server" Text="Multiply the final total by this." CssClass="DieExplanation"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("TotalMultiplier") %>' TextMode="Number" min="1" max="99" Width="100px"></asp:TextBox><asp:RangeValidator ID="RangeValidator8" runat="server" ErrorMessage="Multipliers must range between 1 and 99." ControlToValidate="TextBox6" CssClass="ErrorMessage" MinimumValue="1" MaximumValue="99" Type="Integer"></asp:RangeValidator><br />
                    <asp:Label ID="Label6" runat="server" Text="Multiply the final total by this." CssClass="DieExplanation"></asp:Label>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sort Rolls" SortExpression="SortRolls">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SortTypes" DataTextField="text" DataValueField="value" SelectedValue='<%# Bind("SortRolls") %>' Width="100px"></asp:DropDownList>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SortTypes" DataTextField="text" DataValueField="value" SelectedValue='<%# Bind("SortRolls") %>' Width="100px"></asp:DropDownList>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="Published" HeaderText="Published" SortExpression="Published" />
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>' TextMode="MultiLine" Width="500" Height="100"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Description") %>' TextMode="MultiLine" Width="500" Height="100"></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ShowInsertButton="True" ButtonType="Button" />
        </Fields>
    </asp:DetailsView>
    <em>Note that this form lacks some validation and allows managers to put meaningless values into fields.</em>
    <asp:SqlDataSource ID="SqlDataDie" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" DeleteCommand="DELETE FROM [Die] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Die] ([Name], [Page], [NumDice], [NumSides], [DieAdder], [Description], [Published], [DieType], [DieParameter], [TotalAdder], [TotalMultiplier], [SortRolls], [PagePos]) VALUES (@Name, @Page, @NumDice, @NumSides, @DieAdder, @Description, @Published, @DieType, @DieParameter, @TotalAdder, @TotalMultiplier, @SortRolls, @PagePos)" SelectCommand="SELECT * FROM [Die] WHERE ([Id] = @Id)" UpdateCommand="UPDATE [Die] SET [Name] = @Name, [Page] = @Page, [NumDice] = @NumDice, [NumSides] = @NumSides, [DieAdder] = @DieAdder, [Description] = @Description, [Published] = @Published, [DieType] = @DieType, [DieParameter] = @DieParameter, [TotalAdder] = @TotalAdder, [TotalMultiplier] = @TotalMultiplier, [SortRolls] = @SortRolls, [PagePos] = @PagePos WHERE [Id] = @Id" CacheDuration="600">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Page" Type="Int32" />
            <asp:Parameter Name="NumDice" Type="Int32" />
            <asp:Parameter Name="NumSides" Type="Int32" />
            <asp:Parameter Name="DieAdder" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Published" Type="Boolean" />
            <asp:Parameter Name="DieType" Type="Int16" />
            <asp:Parameter Name="DieParameter" Type="Int32" />
            <asp:Parameter Name="TotalAdder" Type="Int32" />
            <asp:Parameter Name="TotalMultiplier" Type="Int32" />
            <asp:Parameter Name="SortRolls" Type="Int16" />
            <asp:Parameter Name="PagePos" Type="Int16" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="Id" QueryStringField="Id" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Page" Type="Int32" />
            <asp:Parameter Name="NumDice" Type="Int32" />
            <asp:Parameter Name="NumSides" Type="Int32" />
            <asp:Parameter Name="DieAdder" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Published" Type="Boolean" />
            <asp:Parameter Name="DieType" Type="Int16" />
            <asp:Parameter Name="DieParameter" Type="Int32" />
            <asp:Parameter Name="TotalAdder" Type="Int32" />
            <asp:Parameter Name="TotalMultiplier" Type="Int32" />
            <asp:Parameter Name="SortRolls" Type="Int16" />
            <asp:Parameter Name="PagePos" Type="Int16" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlSourcePage" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" SelectCommand="SELECT [Id], RTRIM([Name]) + ' (' + RTRIM([Owner]) + ')' AS [Text] FROM [Page]" CacheDuration="600"></asp:SqlDataSource>
    <asp:XmlDataSource ID="DieTypes" runat="server" DataFile="~/App_Data/DiceTypes.xml"></asp:XmlDataSource>
    <asp:XmlDataSource ID="SortTypes" runat="server" DataFile="~/App_Data/SortTypes.xml"></asp:XmlDataSource>
</asp:Content>

