﻿<%@ Page Title="Page Not Found" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Error404.aspx.cs" Inherits="Errors_Error404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <h1>Page Not Found</h1>
    <p>You've requested a page that doesn't exist, perhaps from an outdated bookmark. Please <a href="~/" runat="server">return to the home page</a>.</p>
</asp:Content>

