﻿<%@ Page Title="An error has occurred" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="OtherErrors.aspx.cs" Inherits="Errors_OtherErrors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <h1>An error has occurred.</h1>
    <p>Sadly, something's gone wrong with this web page. The developer has been notified and hopefully the problem will be remedied shortly.</p>
</asp:Content>

