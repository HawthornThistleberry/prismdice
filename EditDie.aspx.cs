﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PrismDiceModel;
using System.Xml.Linq;

public partial class _EditDie : BasePage
{
    int _IdBeingEdited = 0;
    static int diePage = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString.Get("Id")))
        {
            _IdBeingEdited = Convert.ToInt32(Request.QueryString.Get("Id"));
        }
        if (_IdBeingEdited == 0) {
            Response.Redirect("~/"); // would only happen if someone fakes a URL to try to get here without an Id
        }
        if (!Page.IsPostBack)
        {
            // use EF to load the Id, fill in the fields, update the heading
            using (PrismDiceEntities myEntities = new PrismDiceEntities())
            {
                Die thisDie = (from r in myEntities.Dies
                               where r.Id == _IdBeingEdited
                               select r).SingleOrDefault();
                if (thisDie == null)
                {
                    Response.Redirect("~/"); // maybe they faked a URL with an invalid dice Id
                }

                // update the form heading
                diePage = thisDie.Page;
                PrismDiceModel.Page thisPage = (from r in myEntities.Pages
                                                where r.Id == diePage
                                                select r).SingleOrDefault();
                if (thisPage != null)
                {
                    lblHeading.Text = "<strong>Modify a Die Setting</strong><br />" + thisPage.Name.Trim() + ", position " + thisDie.PagePos.ToString();
                }

                // load the data fields
                txtDieName.Text = (thisDie.Name + "").Trim();
                txtNumDice.Text = thisDie.NumDice.ToString();
                txtNumSides.Text = thisDie.NumSides.ToString();
                txtParameter.Text = thisDie.DieParameter.ToString();
                txtDieAdder.Text = thisDie.DieAdder.ToString();
                txtTotalMultiplier.Text = thisDie.TotalMultiplier.ToString();
                txtTotalAdder.Text = thisDie.TotalAdder.ToString();
                ddlDieType.SelectedIndex = thisDie.DieType; // changed here so the validation won't mess up on the other values
                ddlDieType.DataBind();
                ddlDieType_SelectedIndexChanged(ddlDieType, e);
                ddlSortTypes.SelectedIndex = thisDie.SortRolls;
                chkPublished.Checked = thisDie.Published;
                txtDescription.Text = (thisDie.Description + "").Trim();
            }
        }

    }
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (PrismDiceEntities myEntities = new PrismDiceEntities())
            {
                Die thisDie = (from r in myEntities.Dies
                               where r.Id == _IdBeingEdited
                               select r).SingleOrDefault();
                if (thisDie == null)
                {
                    return;
                }
                thisDie.Name = txtDieName.Text;
                thisDie.DieType = (short)ddlDieType.SelectedIndex;
                thisDie.NumDice = Convert.ToInt32(txtNumDice.Text);
                thisDie.NumSides = Convert.ToInt32(txtNumSides.Text);
                thisDie.DieParameter = Convert.ToInt32(txtParameter.Text);
                thisDie.DieAdder = Convert.ToInt32(txtDieAdder.Text);
                thisDie.TotalMultiplier = Convert.ToInt32(txtTotalMultiplier.Text);
                thisDie.TotalAdder = Convert.ToInt32(txtTotalAdder.Text);
                thisDie.SortRolls = (short)ddlSortTypes.SelectedIndex;
                thisDie.Published = chkPublished.Checked;
                thisDie.Description = txtDescription.Text;
                myEntities.SaveChanges();
            }
            Response.Redirect("~/Default.aspx?Page=" + diePage.ToString());
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/");
    }

    protected void SetValidationLimit(TextBox textField, RangeValidator valField, int low, int high, string msgText)
    {
        // set up the validation limits for a single field; force the value, if necessary, to conform to it.
        int curVal;

        // lownum, lowparam, lowadder==-10000 means disable associated field, set to the equivalent high
        if (low == -10000)
        {
            valField.MinimumValue = high.ToString();
            valField.Enabled = false;
            valField.MaximumValue = high.ToString();
            textField.Text = high.ToString();
            textField.Enabled = false;
        }
        else
        {
            textField.Enabled = true;
            valField.Enabled = true;
            valField.MinimumValue = low.ToString();
            valField.MaximumValue = high.ToString();
            valField.ErrorMessage = msgText + " must range from " + low.ToString() + " to " + high.ToString() + ".";
            curVal = Convert.ToInt32(textField.Text);
            if (curVal < low) textField.Text = low.ToString();
            if (curVal > high) textField.Text = high.ToString();

        }
    }

    protected void SetValidationLimits(int lownum, int highnum, int lowsize, int highsize, int lowparam, int highparam, int lowadder, int highadder, bool disabletotals)
    {
        // set the validation limits to all fields, and handle disabling or enabling the total fields
        SetValidationLimit(txtNumDice, valNumDice, lownum, highnum, "Number of dice");
        SetValidationLimit(txtNumSides, valNumSides, lowsize, highsize, "Number of sides");
        SetValidationLimit(txtParameter, valParameter, lowparam, highparam, "Parameter");
        SetValidationLimit(txtDieAdder, valDieAdder, lowadder, highadder, "Die adder");
        txtTotalAdder.Enabled = !disabletotals;
        txtTotalMultiplier.Enabled = !disabletotals;
        valTotalMultiplier.Enabled = !disabletotals;
        if (disabletotals)
        {
            txtTotalAdder.Text = "0";
            txtTotalMultiplier.Text = "1";
        }
    }

    protected void UpdateValidationLimits()
    {
        // whenever a change is made in die type, number of dice, or number of sides, update all validation limits accordingly
        switch (ddlDieType.SelectedIndex)
        {
            case 1: // Total Rolls
                SetValidationLimits(1, 9999, 2, 9999, -10000, 0, -9999, 9999, false);
                break;
            case 2: // Total Highest
                SetValidationLimits(2, 9999, 2, 9999, 1, Convert.ToInt32(txtNumDice.Text)-1, -9999, 9999, false);
                break;
            case 3: // Total Lowest
                SetValidationLimits(2, 9999, 2, 9999, 1, Convert.ToInt32(txtNumDice.Text)-1, -9999, 9999, false);
                break;
            case 4: // Digit Dice
                SetValidationLimits(2, 12, 2, 10, -10000, 0, -10000, 0, true);
                break;
            case 5: // Open-Ended
                SetValidationLimits(1, 9999, 2, 9999, 1, (Convert.ToInt32(txtNumSides.Text) + 1) / 2 - 1, -9999, 9999, false);
                break;
            case 6: // Open High
                SetValidationLimits(1, 9999, 2, 9999, 1, (Convert.ToInt32(txtNumSides.Text) + 1) / 2 - 1, -9999, 9999, false);
                break;
            case 7: // Open Low
                SetValidationLimits(1, 9999, 2, 9999, 1, (Convert.ToInt32(txtNumSides.Text) + 1) / 2 - 1, -9999, 9999, false);
                break;
            case 8: // Count Under
                SetValidationLimits(1, 9999, 4, 9999, 2, Convert.ToInt32(txtNumSides.Text) - 1, -Convert.ToInt32(txtNumSides.Text) + 1, Convert.ToInt32(txtNumSides.Text)-1, false);
                break;
            case 9: // Count Over
                SetValidationLimits(1, 9999, 4, 9999, 2, Convert.ToInt32(txtNumSides.Text) - 1, -Convert.ToInt32(txtNumSides.Text) + 1, Convert.ToInt32(txtNumSides.Text)-1, false);
                break;
            case 10: // Total Under
                SetValidationLimits(1, 9999, 4, 9999, 2, Convert.ToInt32(txtNumSides.Text) - 1, -9999, 9999, false);
                break;
            case 11: // Total Over
                SetValidationLimits(1, 9999, 4, 9999, 2, Convert.ToInt32(txtNumSides.Text) - 1, -9999, 9999, false);
                break;
            case 12: // Averaging
                SetValidationLimits(1, 9999, 4, 100, 1, Convert.ToInt32(txtNumSides.Text) / 3, -9999, 9999, false);
                break;
            case 13: // Ars Magica
                SetValidationLimits(-10000, 1, 4, 9999, 0, 9999, -9999, 9999, false);
                break;
            case 14: // Champions
                SetValidationLimits(1, 9999, 4, 9999, 0, (Convert.ToInt32(txtNumSides.Text) + 1) / 2 - 1, -9999, 9999, false);
                break;
            case 15: // Series Count
                SetValidationLimits(1, 9999, 2, 9999, 1, Convert.ToInt32(txtNumSides.Text) - 1, -10000, 0, false);
                break;
            case 16: // Divided Dice
                SetValidationLimits(1, 9999, 2, 9999, 1, Convert.ToInt32(txtNumSides.Text) / 2, 0, 9999, false);
                break;
        }
    }

    protected void ddlDieType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            XElement x = XElement.Load(Server.MapPath(@"~/App_Data/DiceTypes.xml"));
            XElement thisDieType = x.Descendants("dicetype")
                                    .Where(r => r.Attribute("diceval").Value.Equals(ddlDieType.SelectedValue))
                                    .FirstOrDefault();
            lblDieTypeDesc.Text = thisDieType.Attribute("dicedesc").Value;
            lblParameterDesc.Text = thisDieType.Attribute("paramdesc").Value;
        }
        catch
        {
            lblDieTypeDesc.Text = "";
            lblParameterDesc.Text = "";
        }
        // update validation limits
        UpdateValidationLimits();
    }
    protected void txtNumDice_TextChanged(object sender, EventArgs e)
    {
        UpdateValidationLimits();
    }
    protected void txtNumSides_TextChanged(object sender, EventArgs e)
    {
        UpdateValidationLimits();
    }

    protected void ddlPublishedDice_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Just shows the description; the load button does the actual work
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            int selectedDie = Convert.ToInt32(ddlPublishedDice.SelectedValue);
            Die thisDie = (from r in myEntities.Dies
                           where r.Id == selectedDie
                           select r).SingleOrDefault();
            if (thisDie != null)
            {
                lblPublishedDieDesc.Text = thisDie.Description.Trim();
            }
            else
            {
                lblPublishedDieDesc.Text = "";
            }
        }
    }

    protected void LoadPublishedDie_Click(object sender, EventArgs e)
    {
        // this one actually loads the settings, but you still have to save them
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            int selectedDie = Convert.ToInt32(ddlPublishedDice.SelectedValue);
            Die thisDie = (from r in myEntities.Dies
                           where r.Id == selectedDie
                           select r).SingleOrDefault();
            if (thisDie != null)
            {
                txtDieName.Text = (thisDie.Name + "").Trim();
                ddlDieType.SelectedIndex = thisDie.DieType;
                ddlDieType.DataBind();
                ddlDieType_SelectedIndexChanged(ddlDieType, e);
                txtNumDice.Text = thisDie.NumDice.ToString();
                txtNumSides.Text = thisDie.NumSides.ToString();
                txtParameter.Text = thisDie.DieParameter.ToString();
                txtDieAdder.Text = thisDie.DieAdder.ToString();
                txtTotalMultiplier.Text = thisDie.TotalMultiplier.ToString();
                txtTotalAdder.Text = thisDie.TotalAdder.ToString();
                ddlSortTypes.SelectedIndex = thisDie.SortRolls;
                chkPublished.Checked = false; // no need to duplicate publication
                txtDescription.Text = (thisDie.Description + "").Trim();
            }
        }
    }
    protected void btnAutomaticName_Click(object sender, EventArgs e)
    {
        string basicDieDesc;

        // build a basic description that most of the specifics will use
        if (Convert.ToInt32(txtNumDice.Text) == 1)
        {
            basicDieDesc = "d";
        }
        else
        {
            basicDieDesc = txtNumDice.Text + "d";
        }
        if (Convert.ToInt32(txtNumSides.Text) == 100)
        {
            basicDieDesc += "%";
        }
        else if (Convert.ToInt32(txtNumSides.Text) == 3 && Convert.ToInt32(txtDieAdder.Text) == -2)
        {
            basicDieDesc += "F";
        }
        else
        {
            basicDieDesc += txtNumSides.Text;
        }

        switch (ddlDieType.SelectedValue)
        {
            case "1": // Total Rolls
                txtDieName.Text = basicDieDesc;
                break;
            case "2": // Total Highest
                txtDieName.Text = basicDieDesc + " highest " + txtParameter.Text;
                break;
            case "3": // Total Lowest
                txtDieName.Text = basicDieDesc + " lowest " + txtParameter.Text;
                break;
            case "4": // Digit Dice
                if (basicDieDesc == "3d6")
                {
                    txtDieName.Text = "In Nomine";
                }
                else
                {
                    txtDieName.Text = basicDieDesc + " digits";
                }
                break;
            case "5": // Open-Ended
                if (basicDieDesc == "d%" && Convert.ToInt32(txtParameter.Text) == 5)
                {
                    txtDieName.Text = "Rolemaster";
                }
                else
                {
                    txtDieName.Text = basicDieDesc + " open";
                }
                break;
            case "6": // Open High
                if (basicDieDesc == "d%" && Convert.ToInt32(txtParameter.Text) == 5)
                {
                    txtDieName.Text = "Rolemaster High";
                }
                else
                {
                    txtDieName.Text = basicDieDesc + " open high";
                }
                break;
            case "7": // Open Low
                txtDieName.Text = basicDieDesc + " open low";
                break;
            case "8": // Count Under
                txtDieName.Text = basicDieDesc + " count under " + txtParameter.Text;
                break;
            case "9": // Count Over
                txtDieName.Text = basicDieDesc + " count over " + txtParameter.Text;
                break;
            case "10": // Total Under
                txtDieName.Text = basicDieDesc + " total under " + txtParameter.Text;
                break;
            case "11": // Total Over
                txtDieName.Text = basicDieDesc + " total over " + txtParameter.Text;
                break;
            case "12": // Averaging
                txtDieName.Text = "Averaging " + basicDieDesc;
                break;
            case "13": // Ars Magica
                txtDieName.Text = "Ars Magica";
                break;
            case "14": // Champions
                if (Convert.ToInt32(txtParameter.Text) == 0)
                {
                    txtDieName.Text = "Champions Killing";
                }
                else
                {
                    txtDieName.Text = "Champions";
                }
                break;
            case "15": // Series Count
                txtDieName.Text = "Count " + basicDieDesc + " under " + txtParameter.Text;
                break;
            case "16": // Divided Dice
                txtDieName.Text = basicDieDesc + "÷d" + txtParameter.Text;
                break;
        }
    }
}