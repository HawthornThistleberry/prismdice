﻿<%@ Page Title="Create an account" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="CreateAccount.aspx.cs" Inherits="CreateAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="3px">
        <div align="center"><asp:CreateUserWizard ID="CreateUserWizard1" runat="server" CompleteSuccessText="Congratulations, you have an account now." ConfirmPasswordCompareErrorMessage="The passwords don't match." ConfirmPasswordRequiredErrorMessage="Please confirm your password." DuplicateEmailErrorMessage="The email address that you entered is already in use. Please enter a different e-mail address." DuplicateUserNameErrorMessage="That user name is taken." EmailLabelText="Email:" EmailRegularExpressionErrorMessage="Please enter a different email." EmailRequiredErrorMessage="Email is required." InvalidEmailErrorMessage="Please enter a valid email address." UserNameLabelText="Username:" UserNameRequiredErrorMessage="Username is required." CreateUserButtonText="Create Account">
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server" Title="Sign Up" />
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" />
        </WizardSteps>
    </asp:CreateUserWizard></div>
        <div align="center"><asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" /></div>
        <br />
        <em>Creating an account will (eventually) allow you to customize dice buttons, make pages of dice, share your dice with other users, and import dice shared by others. Your information will be kept confidential (apart from any of your custom dice you choose to share) and will not result in any promotional or other unsolicited contact. </em>
        </asp:Panel>
</asp:Content>

