﻿<%@ Page Title="Recover a forgotten password" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="_ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="3px">
        <div align="center">
            <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" UserNameInstructionText="Enter your username to receive your password." UserNameLabelText="Username:" UserNameTitleText="Need a new password?">
                <MailDefinition Subject="Your new password for PrismDice.azurewebsites.net"></MailDefinition>
            </asp:PasswordRecovery>
        </div>
        <div align="center"><asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" /></div>
        <br />
        <em>You will receive your new password via the email address you used when first configuring your account. You can use it to log in and then change your password once more.</em>
        </asp:Panel>
</asp:Content>

