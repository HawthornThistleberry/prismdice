﻿<%@ Page Title="Prism Dice" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <script type="text/javascript">
        $(function () {
            // if this was called with a ?Page=# in the URL, strip it in the bar
            history.pushState('', 'Prism Dice', 'Default.aspx');

            // the PageEdit button should toggle the visibility of the page edit functions
            function togglePageEdit() {
                $('#divPageEdit').slideToggle();
                return false;
            }
            $('#PagesEdit').on("click", togglePageEdit);

            // the Delete button does nothing but expose the real delete button and a cancel
            function displayDeleteConfirmation() {
                $('#deleteconfirm').slideDown();
                return false;
            }
            $('#DeleteButton').on("click", displayDeleteConfirmation);

            // the "no" button in the delete panel just hides what the Delete button showed
            function hideDeleteConfirmation() {
                $('#deleteconfirm').slideUp();
                return false;
            }
            $('#CancelDelete').on("click", hideDeleteConfirmation);

            // this validation function fires on the Create or Rename button and prevents the button from submitting if there's not a name in txtPages, so the error is more visible; if someone has JavaScript disabled, they won't even be seeing these fields, so it really doesn't matter, but just in case, the fields are also being validated server-side.
            function pageNameRequired() {
                var pageName = document.getElementById('<%= txtPageName.ClientID %>');
                if (pageName.value == '') {
                    alert('A name must be specified.');
                    return false;
                } else {
                    return true;
                }
            }
            $('#btnRename').on("click", pageNameRequired);
            $('#btnCreate').on("click", pageNameRequired);

            // on an UpdatePanel postback all the bindings is lost, so rebind everything on reload
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandle);
            function endRequestHandle(sender, Args) {
                $('#PagesEdit').on("click", togglePageEdit);
                $('#DeleteButton').on("click", displayDeleteConfirmation);
                $('#CancelDelete').on("click", hideDeleteConfirmation);
                $('#btnRename').on("click", pageNameRequired);
                $('#btnCreate').on("click", pageNameRequired);
            }

        });
    </script>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="DiceCalc">
        <ProgressTemplate>
            <div class="PleaseWait">Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="DiceCalc" runat="server">
    <ContentTemplate>
    <table id="DiceCalcTable" class="DiceCalcTable">
        <tr>
            <td colspan="6">
                <asp:Label ID="ResultHeader" runat="server" CssClass="Result"></asp:Label>
                <asp:Label ID="Result" runat="server" CssClass="Result" Text="0"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Label ID="ResultRolls" runat="server"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlPage" CssClass="DiceButton" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged"></asp:DropDownList>
                <asp:Button ID="PagesEdit" runat="server" Text="◊" CssClass="EditButton" ClientIDMode="Static" />
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="CalcStore1" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall1" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore2" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall2" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore3" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall3" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore4" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall4" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
        </tr>
        <tr style="padding: 0;">
            <td colspan="6" style="padding: 0;">
                <div id="divPageEdit" style="display: none;">
                    <asp:TextBox ID="txtPageName" runat="server"></asp:TextBox> <asp:Button ID="btnRename" runat="server" Text="Rename" Enabled="False" ValidationGroup="RenameCreate" OnClick="btnRename_Click" ClientIDMode="Static" /><asp:Button ID="btnCreate" runat="server" Text="Create" ValidationGroup="RenameCreate" OnClick="btnCreate_Click" ClientIDMode="Static" /> <input id="DeleteButton" type="button" value="Delete" /> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="A name must be specified." ControlToValidate="txtPageName" runat="server" ValidationGroup="RenameCreate" CssClass="ErrorMessage" Display="Dynamic" SetFocusOnError="True" /><div id="deleteconfirm" style="display: none;"><span class="ErrorMessage">Really delete these dice settings permanently?</span> <asp:Button ID="btnDelete" runat="server" Text="Yes" Enabled="False" OnClick="btnDelete_Click" /><input id="CancelDelete" type="button" value="No" /></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice1" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit1" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td rowspan="8">&nbsp;</td>
            <td>
                <asp:Button ID="CalcStore5" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall5" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore6" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall6" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore7" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall7" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore8" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall8" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice2" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit2" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td>
                <asp:Button ID="CalcStore9" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall9" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore10" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall10" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore11" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall11" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
            <td>
                <asp:Button ID="CalcStore12" runat="server" CssClass="CalcStoreButton" OnClick="CalcStore_Click" Text="»" />
                <asp:Button ID="CalcRecall12" runat="server" CssClass="CalcRecallButton" OnClick="CalcRecall_Click" Text="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice3" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit3" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td colspan="2">
                <asp:Button ID="CalcBackspace" runat="server" CssClass="CalcButton2" Text="←" OnClick="CalcBackspace_Click" />
            </td>
            <td colspan="2">
                <asp:Button ID="CalcClear" runat="server" CssClass="CalcButton2" OnClick="CalcClear_Click" Text="Clear" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice4" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit4" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td colspan="2">
                <asp:Button ID="Dice0" runat="server" CssClass="CalcButton2" Text="Repeat" Enabled="False" OnClick="DiceRoll" />
            </td>
            <td>
                <asp:Button ID="CalcSign" runat="server" CssClass="CalcButton" Text="±" OnClick="CalcSign_Click" />
            </td>
            <td>
                <asp:Button ID="CalcPower" runat="server" CssClass="CalcButton" Text="^" OnClick="CalcPower_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice5" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit5" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc7" runat="server" Text="7" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc8" runat="server" Text="8" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc9" runat="server" Text="9" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="CalcDivide" runat="server" Text="÷" CssClass="CalcButton" OnClick="CalcDivide_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice6" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit6" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc4" runat="server" Text="4" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc5" runat="server" Text="5" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc6" runat="server" Text="6" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="CalcMultiply" runat="server" Text="×" CssClass="CalcButton" OnClick="CalcMultiply_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice7" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit7" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc1" runat="server" Text="1" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc2" runat="server" Text="2" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc3" runat="server" Text="3" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td>
                <asp:Button ID="CalcSubtract" runat="server" Text="-" CssClass="CalcButton" OnClick="CalcSubtract_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Dice8" runat="server" Text="" OnClick="DiceRoll" CssClass="DiceButton" />
                <asp:Button ID="DiceEdit8" runat="server" Text="◊" CssClass="EditButton" Enabled="False" OnClick="DiceEdit_Click" />
            </td>
            <td>
                <asp:Button ID="Calc0" runat="server" Text="0" CssClass="CalcButton" OnClick="CalcDigit_Click" />
            </td>
            <td colspan="2">
                <asp:Button ID="CalcEquals" runat="server" Text="=" CssClass="CalcButton2" OnClick="CalcEquals_Click" />
            </td>
            <td>
                <asp:Button ID="CalcAdd" runat="server" Text="+" CssClass="CalcButton" OnClick="CalcAdd_Click" />
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

