//#define CACHE_DICE
// Caching of dice has been commented out because I can't set up a working SqlCacheDependency while on Azure's free trial.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PrismDiceModel;
using System.Web.Caching;

public partial class _Default : BasePage
{
    // variables used for the repeat die button
    static int repeat_quantityoverride = 1;
    static Die repeat_Die = null;

    // variables used for the calculator and display
    static int first_operand = 0;
    static Operator last_operator = Operator.NoOperation;
    static bool clear_rolls = true;

    // variables used for the current page load
    static int currentPage = 0;
    static bool forcePageLoad = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString.Get("Page")))
            {
                currentPage = Convert.ToInt32(Request.QueryString.Get("Page"));
                forcePageLoad = true;
            }
            // Load the dice page dropdown with pages only for the currently logged in user
            DropDownList ddlPage = (DropDownList)DiceCalc.FindControl("ddlPage");
            if (ddlPage != null)
            {
                using (PrismDiceEntities myEntities = new PrismDiceEntities())
                {
                    if ((System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    { // if someone is logged in, find their dice pages
                        var myPages = from r in myEntities.Pages
                                      where r.Owner.Trim() == User.Identity.Name.Trim()
                                      select r;
                        if (myPages != null)
                        {
                            foreach (PrismDiceModel.Page myPage in myPages)
                            {
                                ddlPage.Items.Add(new ListItem(myPage.Name, myPage.Id.ToString()));
                            }
                        }
                    }
                }
                // The default page should always be on the list at the bottom; this also means there's always something on the list.
                ddlPage.Items.Add(new ListItem("Default", Find_Default_Page().ToString()));
                try
                {
                    if (forcePageLoad == false)
                    {
                        ddlPage.SelectedValue = Profile.LastDicePage.ToString();
                    }
                    else
                    {
                        ddlPage.SelectedValue = currentPage.ToString();
                    }
                }
                catch
                {
                    // do nothing
                }
            }
        }
        ddlPage_SelectedIndexChanged(sender, e);
    }

    // Find the "Default" page, which is usually 1 but let's not assume that
    protected int Find_Default_Page()
    {
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            var thisPage = (from r in myEntities.Pages
                           where r.Name == "Default"
                           select r).SingleOrDefault();
            // if we can't find it, let's just grab the first page we can see and go with that, better than nothing
            if (thisPage == null)
            {
                thisPage = (from r in myEntities.Pages
                            select r).SingleOrDefault();
            }
            // and if we still don't have one... there's no pages, and no good can come of this
            if (thisPage == null) return 0;
            return (int)thisPage.Id;
        }
    }

    // Given a page and position, return the Id from the Die table that corresponds, or 0 if none is defined
    protected Die Find_Die_On_Page(int thePage, short position)
    {
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            Die thisDie;
#if CACHE_DICE
            // load it from cache if it's there
            thisDie = Cache["Die_" + thePage.ToString() + "_" + position.ToString()] as Die;
            if (thisDie == null)
            {
#endif
                // try loading it directly
                thisDie = (from r in myEntities.Dies
                           where r.Page == thePage && r.PagePos == position
                           select r).SingleOrDefault();
#if CACHE_DICE
            if (thisDie != null)
                { // and if successful, cache it for next time
                    Cache.Insert("Die_" + thePage.ToString() + "_" + position.ToString(), thisDie);
                }
            }
#endif
            return thisDie;
        }
    }

    // As above, but if the page or position is vacant, fill in from the default page
    // This shouldn't happen if other parts of the application are doing their job, creating pages correctly, but it's a good safeguard.
    protected Die Find_Die_On_Page_Or_Default(int thePage, short position)
    {
        Die actualDie = Find_Die_On_Page(thePage, position);
        if (actualDie != null) return actualDie;
        actualDie = Find_Die_On_Page(Find_Default_Page(), position);
        return actualDie;
    }

    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        int newPage;

        // which is the selected dice page?
        try
        {
            newPage = Convert.ToInt32(ddlPage.SelectedValue);
        }
        catch
        {
            newPage = Find_Default_Page();
        }

        // if those are not currently loaded, load them
        // (not doing so if not needed is a huge performance improvement)
        if (forcePageLoad || newPage != currentPage || Dice1.Text == "")
        {
            Load_Dice_Page(newPage);
        }
    }

    // Load a page of dice
    protected void Load_Dice_Page(int loaded_page)
    {
        int defaultPage = Find_Default_Page();

        // record this for later lookups
        currentPage = loaded_page;

        // save it in the profile as the default page
        if ((System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
        {
            btnCreate.Enabled = true;
            Profile.LastDicePage = loaded_page;
            if (loaded_page != defaultPage)
            {
                btnDelete.Enabled = true;
                btnRename.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
                btnRename.Enabled = false;
            }
        }
        else
        {
            PagesEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnRename.Enabled = false;
            btnCreate.Enabled = false;
        }


        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            // for each position...
            short i;
            for (i = 1; i <= 8; i++)
            {
                // retrieve the button in that position, caching as you go
                Die thisDie = Find_Die_On_Page_Or_Default(loaded_page, i);
                if (thisDie == null) continue; // should only happen if the Die table got munged so badly there's no Default page contents

                // set the dice label accordingly
                Button thisDieButton = (Button) DiceCalc.FindControl("Dice" + i.ToString());
                if (thisDieButton != null)
                {
                    // set the button text
                    thisDieButton.Text = thisDie.Name.Trim();

                    // hide or show the associated edit button depending on whether it's the Default page or you're a manager
                    Button editDieButton = (Button)DiceCalc.FindControl("DiceEdit" + i.ToString());
                    if (editDieButton != null)
                    {
                        if (thisDie.Page == defaultPage && !User.IsInRole("Manager"))
                        {
                            editDieButton.Enabled = false;
                        }
                        else
                        {
                            editDieButton.Enabled = true;
                        }
                    }
                }
            }
        }
    }

    protected void btnRename_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (PrismDiceEntities myEntities = new PrismDiceEntities())
            {
                var thisPage = (from r in myEntities.Pages
                                where r.Id == currentPage
                                select r).SingleOrDefault();
                thisPage.Name = txtPageName.Text;
                myEntities.SaveChanges();
                Response.Redirect("~/Default.aspx?Page=" + currentPage.ToString());
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            // delete each die on this page
            var theseDice = from r in myEntities.Dies
                            where r.Page == currentPage
                            select r;
            foreach (var thisDie in theseDice)
            {
                myEntities.Dies.DeleteObject(thisDie);
            }

            // delete the page itself
            var thesePages = from r in myEntities.Pages
                             where r.Id == currentPage
                             select r;
            foreach (var thisPage in thesePages)
            {
                myEntities.Pages.DeleteObject(thisPage);
            }

            // save the deletions
            try
            {
                myEntities.SaveChanges();
            }
            catch
            {
                // do nothing
            }
        }

        // switch back to the default page
        Response.Redirect("~/Default.aspx?Page=" + Find_Default_Page().ToString());
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (PrismDiceEntities myEntities = new PrismDiceEntities())
            {
                // create a page of that name
                PrismDiceModel.Page newPage = new PrismDiceModel.Page();
                newPage.Name = txtPageName.Text;
                newPage.Owner = User.Identity.Name;

                myEntities.Pages.AddObject(newPage);
                myEntities.SaveChanges();

                // populate it with copies of the defaults
                int defaultPage = Find_Default_Page();
                var theseDice = from r in myEntities.Dies
                                where r.Page == defaultPage
                                select r;
                foreach (var thisDie in theseDice.ToList())
                {
                    Die newDie = new Die();
                    newDie.Name = (thisDie.Name + "").Trim();
                    newDie.Page = newPage.Id;
                    newDie.PagePos = thisDie.PagePos;
                    newDie.DieType = thisDie.DieType;
                    newDie.NumDice = thisDie.NumDice;
                    newDie.NumSides = thisDie.NumSides;
                    newDie.DieParameter = thisDie.DieParameter;
                    newDie.DieAdder = thisDie.DieAdder;
                    newDie.TotalMultiplier = thisDie.TotalMultiplier;
                    newDie.TotalAdder = thisDie.TotalAdder;
                    newDie.SortRolls = thisDie.SortRolls;
                    newDie.Published = false;
                    newDie.Description = (thisDie.Description + "").Trim();
                    myEntities.Dies.AddObject(newDie);
                    myEntities.SaveChanges(); //-- this is where it fails with an unhandled exception
                }

                // switch to the new page
                Response.Redirect("~/Default.aspx?Page=" + newPage.Id.ToString()); //-- isn't going to the right page
            }
        }
    }

    // This function handles all the work of figuring out what we're rolling, and handling the results,
    // and uses the above functions for actually rolling the dice
    protected void DiceRoll(object sender, EventArgs e)
    {
        DieRolls myDiceRoller = new DieRolls();
        Button thisButton = (Button)sender;
        short position = Convert.ToInt16(thisButton.ID.Substring(4));

        Die thisDie;

        if (position == 0) // this means we're doing the Repeat button
        {
            thisDie = repeat_Die;
            // repeat_quantityoverride is simply allowed to stand
            ResultHeader.Text = Dice0.Text;
        }
        else // we're using a fresh die so look up everything
        {
            // find the die settings
            thisDie = Find_Die_On_Page_Or_Default(currentPage, position);

            // figure out the quantity of dice to roll
            if (thisDie.NumDice > 1)
            { // if a die definition specifies a number of dice, always use it
                repeat_quantityoverride = thisDie.NumDice;
                ResultHeader.Text = "";
            }
            else
            { // if it doesn't specify, allow the user to use the calculator to enter a quantity and then hit a die button
                repeat_quantityoverride = Convert.ToInt32(Result.Text);
                if (repeat_quantityoverride < 1 || repeat_quantityoverride > 999 || ResultHeader.Text != "") repeat_quantityoverride = 1;
                if (repeat_quantityoverride > 1)
                {
                    ResultHeader.Text = repeat_quantityoverride.ToString();
                    if (thisDie.Name[0] != 'd') ResultHeader.Text += " ";
                }
                else ResultHeader.Text = "";
            }

            // finish setting up the result header and store other parameters for reuse
            ResultHeader.Text += thisDie.Name.Trim();
            repeat_Die = thisDie;
            Dice0.Text = ResultHeader.Text;
            Dice0.Enabled = true;
        }

        if (thisDie == null)
        { // should never happen, so many safeguards before this point, but...
            Result.Text = "0";
            ResultRolls.Text = "Error, undefined die!";
            return;
        }

        ResultHeader.Text += " = ";
        ResultRolls.Text = "";

        switch (thisDie.DieType)
        {
            case 1: // Total Rolls
            case 8: // Count Under
            case 9: // Count Over
            case 5: // Open-Ended
            case 6: // Open High
            case 7: // Open Low
            case 10: // Total Under
            case 11: // Total Over
                myDiceRoller.DiceRoll_Total(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
            case 2: // Total Highest
            case 3: // Total Lowest
                myDiceRoller.DiceRoll_TotalSorted(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
            case 4: // Digit Dice
                myDiceRoller.DiceRoll_DigitDice(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
            case 12: // Averaging
                myDiceRoller.DiceRoll_Averaging(repeat_quantityoverride, thisDie, Result, ResultRolls, Cache);
                break;
            case 13: // Ars Magica
                myDiceRoller.DiceRoll_ArsMagica(thisDie, Result, ResultRolls);
                break;
            case 14: // Champions
                myDiceRoller.DiceRoll_Champions(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
            case 15: // Series Count
                myDiceRoller.DiceRoll_SeriesCount(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
            case 16: // Divided Dice
                myDiceRoller.DiceRoll_DividedDice(repeat_quantityoverride, thisDie, Result, ResultRolls);
                break;
        }
    }

    // Edit buttons
    protected void DiceEdit_Click(object sender, EventArgs e)
    {
        Button thisButton = (Button)sender;
        int position = Convert.ToInt32(thisButton.ID.Substring(8));
        using (PrismDiceEntities myEntities = new PrismDiceEntities())
        {
            Die thisDie = (from r in myEntities.Dies
                           where r.Page == currentPage && r.PagePos == position
                           select r).SingleOrDefault();
            if (thisDie != null)
            {
                Response.Redirect("EditDie.aspx?Id=" + thisDie.Id.ToString());
            }
        }

    }

    // Calculator numbers
    protected void CalcDigit_Click(object sender, EventArgs e)
    {
        Button thisButton = (Button)sender;
        int digit = Convert.ToInt32(thisButton.ID.Substring(4));

        if (ResultHeader.Text == "" && Result.Text != "0")
        { // just append to whatever is there
            Result.Text = Result.Text + digit.ToString();
        }
        else
        { // clear and replace
            ResultHeader.Text = "";
            if (clear_rolls) ResultRolls.Text = "";
            Result.Text = digit.ToString();
        }
    }
    protected void CalcSign_Click(object sender, EventArgs e)
    {
        ResultHeader.Text = "";
        if (clear_rolls) ResultRolls.Text = "";
        int curvalue = Convert.ToInt32(Result.Text);
        if (curvalue == 0)
        {
            Result.Text = "0";
        }
        else
        {
            curvalue *= -1;
            Result.Text = curvalue.ToString();
        }
    }

    // Edit functions for the display
    protected void CalcClear_Click(object sender, EventArgs e)
    {
        ResultHeader.Text = "";
        ResultRolls.Text = "";
        Result.Text = "0";
        first_operand = 0;
        last_operator = Operator.NoOperation;
    }
    protected void CalcBackspace_Click(object sender, EventArgs e)
    {
        ResultHeader.Text = "";
        if (clear_rolls) ResultRolls.Text = "";
        if (Result.Text.Length == 1)
        {
            Result.Text = "0";
        }
        else
        {
            Result.Text = Result.Text.Remove(Result.Text.Length-1, 1);
        }
    }

    // Memory store functions use the button text itself as data storage
    protected void CalcStore_Click(object sender, EventArgs e)
    {
        Button thisButton = (Button)sender;
        int buttonNumber = Convert.ToInt32(thisButton.ID.Substring(9));
        Button recallButton = (Button)DiceCalc.FindControl("CalcRecall" + buttonNumber.ToString());
        recallButton.Text = Result.Text;
    }

    // And recall do the opposite
    protected void CalcRecall_Click(object sender, EventArgs e)
    {
        Button thisButton = (Button)sender;
        int buttonNumber = Convert.ToInt32(thisButton.ID.Substring(10));
        ResultHeader.Text = "Memory " + buttonNumber.ToString() + " =";
        if (clear_rolls) ResultRolls.Text = "";
        Result.Text = thisButton.Text;
    }

    // calculation buttons
    protected void Calculation_Execute()
    {
        int new_result;
        new_result = Convert.ToInt32(Result.Text);
        if (last_operator == Operator.Add)
        {
            ResultHeader.Text = first_operand.ToString() + " + " + new_result.ToString() + " = ";
            new_result += first_operand;
        }
        else if (last_operator == Operator.Subtract)
        {
            ResultHeader.Text = first_operand.ToString() + " - " + new_result.ToString() + " = ";
            new_result = first_operand - new_result;
        }
        else if (last_operator == Operator.Multiply)
        {
            ResultHeader.Text = first_operand.ToString() + " � " + new_result.ToString() + " = ";
            new_result *= first_operand;
        }
        else if (last_operator == Operator.Divide)
        {
            ResultHeader.Text = first_operand.ToString() + " � " + new_result.ToString() + " = ";
            if (new_result == 0)
            {
                new_result = 0;
                ResultRolls.Text = "Divide by zero error!";
            }
            else
            {
                new_result = first_operand / new_result;
            }
        }
        else if (last_operator == Operator.Power)
        {
            ResultHeader.Text = first_operand.ToString() + " ^ " + new_result.ToString() + " = ";
            new_result = (int)Math.Pow((double)first_operand, (double)new_result);
        }
        Result.Text = new_result.ToString();
    }

    protected void CalcEquals_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
            first_operand = 0;
            last_operator = Operator.NoOperation;
            clear_rolls = true;
            if (ResultRolls.Text != "Divide by zero error!") ResultRolls.Text = "";
        }
    }
    protected void CalcAdd_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
        }
        first_operand = Convert.ToInt32(Result.Text);
        last_operator = Operator.Add;
        Result.Text = "0";
        ResultHeader.Text = "";
        ResultRolls.Text = first_operand.ToString() + " + ";
        clear_rolls = false;
    }
    protected void CalcSubtract_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
        }
        first_operand = Convert.ToInt32(Result.Text);
        last_operator = Operator.Subtract;
        Result.Text = "0";
        ResultHeader.Text = "";
        ResultRolls.Text = first_operand.ToString() + " - ";
        clear_rolls = false;
    }
    protected void CalcMultiply_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
        }
        first_operand = Convert.ToInt32(Result.Text);
        last_operator = Operator.Multiply;
        Result.Text = "0";
        ResultHeader.Text = "";
        ResultRolls.Text = first_operand.ToString() + " � ";
        clear_rolls = false;
    }
    protected void CalcDivide_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
        }
        first_operand = Convert.ToInt32(Result.Text);
        last_operator = Operator.Divide;
        Result.Text = "0";
        ResultHeader.Text = "";
        ResultRolls.Text = first_operand.ToString() + " � ";
        clear_rolls = false;
    }
    protected void CalcPower_Click(object sender, EventArgs e)
    {
        if (last_operator != Operator.NoOperation)
        {
            Calculation_Execute();
        }
        first_operand = Convert.ToInt32(Result.Text);
        last_operator = Operator.Power;
        Result.Text = "0";
        ResultHeader.Text = "";
        ResultRolls.Text = first_operand.ToString() + " ^ ";
        clear_rolls = false;
    }

}