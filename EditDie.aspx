﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="EditDie.aspx.cs" Inherits="_EditDie" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
    .auto-style3 {
        width: 139px;
        text-align: right;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <table class="auto-style1">
        <tr>
            <td colspan="3" style="text-align: center">
                <asp:Label ID="lblHeading" runat="server" Text="Modify a Die Setting"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="3"><hr /></td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label1" runat="server" Text="Load Settings" AssociatedControlID="ddlPublishedDice"></asp:Label></td>
            <td>
                <asp:DropDownList ID="ddlPublishedDice" runat="server" AutoPostBack="True" DataSourceID="sqlPublishedDice" DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="ddlPublishedDice_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sqlPublishedDice" runat="server" ConnectionString="<%$ ConnectionStrings:PrismDiceConnectionString1 %>" SelectCommand="SELECT [Id], [Name], [Description] FROM [Die] WHERE ([Published] = @Published)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="true" Name="Published" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
&nbsp;<asp:Button ID="LoadPublishedDie" runat="server" Text="Load" OnClick="LoadPublishedDie_Click" />
            </td>
            <td style="font-style: italic">
                <asp:Label ID="lblPublishedDieDesc" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr /></td>
        </tr>
        <tr>
            <td class="auto-style3"><asp:Label ID="Label2" runat="server" Text="Name" AssociatedControlID="txtDieName"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDieName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="valDieName" runat="server" ControlToValidate="txtDieName" CssClass="ErrorMessage" ErrorMessage="A die name is required." Display="Dynamic" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="btnAutomaticName" runat="server" Text="Automatically generate a name" OnClick="btnAutomaticName_Click" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label3" runat="server" Text="Die Type" AssociatedControlID="ddlDieType"></asp:Label></td>
            <td>
                <asp:DropDownList ID="ddlDieType" runat="server" AutoPostBack="True" DataSourceID="xmlDiceTypes" DataTextField="dicename" DataValueField="diceval" OnSelectedIndexChanged="ddlDieType_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:XmlDataSource ID="xmlDiceTypes" runat="server" DataFile="~/App_Data/DiceTypes.xml"></asp:XmlDataSource>
            </td>
            <td>
                <i>
                <asp:Label ID="lblDieTypeDesc" runat="server"></asp:Label>
                </i>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label4" runat="server" Text="Number of Dice" AssociatedControlID="txtNumDice"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtNumDice" runat="server" TextMode="Number" OnTextChanged="txtNumDice_TextChanged"></asp:TextBox>
                <asp:RangeValidator ID="valNumDice" runat="server" ControlToValidate="txtNumDice" CssClass="ErrorMessage" ErrorMessage="Number of dice must be in the range 1 to 999." MaximumValue="999" MinimumValue="1" Display="Dynamic" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
            <td><i>Set to 1 to allow the calculator to choose.</i></td>
        </tr>
        <tr>
            <td class="auto-style3">Number of Sides</td>
            <td>
                <asp:TextBox ID="txtNumSides" runat="server" TextMode="Number" OnTextChanged="txtNumSides_TextChanged"></asp:TextBox>
                <asp:RangeValidator ID="valNumSides" runat="server" ControlToValidate="txtNumSides" CssClass="ErrorMessage" ErrorMessage="Number of sides must range from 1 to 9999." MaximumValue="9999" MinimumValue="1" Display="Dynamic" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
            <td><i>The size of the die being rolled.</i></td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label5" runat="server" Text="Parameter" AssociatedControlID="txtParameter"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtParameter" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RangeValidator ID="valParameter" runat="server" ControlToValidate="txtParameter" CssClass="ErrorMessage" ErrorMessage="RangeValidator" Display="Dynamic" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
            <td>
                <i>
                <asp:Label ID="lblParameterDesc" runat="server"></asp:Label>
                </i>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label6" runat="server" Text="Die Adder"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDieAdder" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RangeValidator ID="valDieAdder" runat="server" ControlToValidate="txtDieAdder" CssClass="ErrorMessage" ErrorMessage="RangeValidator" MaximumValue="9999" MinimumValue="-9999" Display="Dynamic" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
            <td><i>This value is added to each die rolled.</i></td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label7" runat="server" Text="Total Multiplier" AssociatedControlID="txtTotalMultiplier"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTotalMultiplier" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RangeValidator ID="valTotalMultiplier" runat="server" ControlToValidate="txtTotalMultiplier" CssClass="ErrorMessage" ErrorMessage="Total multiplier must range from 1 to 99." MaximumValue="99" MinimumValue="1" Display="Dynamic" SetFocusOnError="True" Type="Integer">*</asp:RangeValidator>
            </td>
            <td><i>The final total is multiplied by this amount.</i></td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label8" runat="server" Text="Total Adder" AssociatedControlID="txtTotalAdder"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTotalAdder" runat="server" TextMode="Number"></asp:TextBox>
            </td>
            <td><i>Added to the final total after multiplication.</i></td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label9" runat="server" Text="Sort Rolls" AssociatedControlID="ddlSortTypes"></asp:Label></td>
            <td>
                <asp:DropDownList ID="ddlSortTypes" runat="server" DataSourceID="xmlSortTypes" DataTextField="text" DataValueField="value">
                </asp:DropDownList>
                <asp:XmlDataSource ID="xmlSortTypes" runat="server" DataFile="~/App_Data/SortTypes.xml"></asp:XmlDataSource>
            </td>
            <td><i></i></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td colspan="2">
                <asp:CheckBox ID="chkPublished" runat="server" Text="Publish die for other users" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="Label10" runat="server" Text="Description" AssociatedControlID="txtDescription"></asp:Label></td>
            <td colspan="2">
                <asp:TextBox ID="txtDescription" runat="server" Rows="5" TextMode="MultiLine" Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr /></td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
&nbsp;<asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="ErrorMessage" />
            </td>
        </tr>
    </table>
</asp:Content>

