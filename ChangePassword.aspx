﻿<%@ Page Title="Change Password or Email Address" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="3px">
        <div align="center">
            <asp:LoginView ID="LoginView1" runat="server">
                <AnonymousTemplate>
                    You need to <a href="~/Login.aspx">log in</a> before you can change your password or email address.
                </AnonymousTemplate>
                <LoggedInTemplate>
                    <asp:ChangePassword ID="ChangePassword1" runat="server" UserNameLabelText="Username:"></asp:ChangePassword>
                    <hr />
                    <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox> <asp:Button ID="ChangeEmail" runat="server" Text="Change Email" OnClick="ChangeEmail_Click" /><br />
                    <asp:Label ID="EmailChangeMessage" runat="server" Text="" CssClass="ErrorMessage"></asp:Label>
                </LoggedInTemplate>
            </asp:LoginView>
        </div>
        <br />
        <em></em>
        </asp:Panel>
</asp:Content>

