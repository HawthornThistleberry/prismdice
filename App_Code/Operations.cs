﻿public enum Operator
{
    NoOperation,
    Add,
    Subtract,
    Multiply,
    Divide,
    Power
}