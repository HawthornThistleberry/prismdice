﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrismDiceModel;
using System.Web.UI.WebControls;
using System.Web.Caching;

/// <summary>
/// Summary description for DieRolls
/// </summary>
public class DieRolls
{
	public DieRolls()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    // Assembles an int and string into a sortable string by padding the int to 9 digits
    // What I should have done is used a struct as described here: http://msdn.microsoft.com/en-us/library/w56d4y5z(v=vs.110).aspx
    protected string DiceResults_Assemble(int roll, string resulttext)
    {
        return roll.ToString("D9") + " " + resulttext;
    }

    // Implements any necessary sorting and then builds the ResultsRolls.Text field
    protected void BuildResultRolls(short sorttype, List<string> results, Label ResultRolls) //List<int> result_rolls, List<string> result_texts)
    {
        ResultRolls.Text = "";
        if (sorttype == 1)
        {
            results.Sort();
        }
        if (sorttype == 2)
        {
            results.Sort(new Comparison<string>((s1, s2) => s2.CompareTo(s1)));
        }
        foreach (string r in results)
        {
            if (ResultRolls.Text != "") ResultRolls.Text += ", ";
            ResultRolls.Text += r.Substring(10);
        }
    }

    public void DiceRoll_Total(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int dieresult = 0, i, thisroll;
        Random r = new Random();

        // variables used for the sortable list of rolls and results
        List<string> results = new List<string>();
        string result_text = "";

        for (i = 0; i < qty; i++)
        {
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
            switch (thisDie.DieType)
            {
                case 1: // Total Rolls
                    dieresult += thisroll + thisDie.DieAdder;
                    results.Add(DiceResults_Assemble(dieresult, dieresult.ToString()));
                    break;
                case 2: // Total Highest
                    break;
                case 3: // Total Lowest
                    break;
                case 5: // Open-Ended
                    dieresult += thisroll + thisDie.DieAdder;
                    result_text = dieresult.ToString();
                    if (dieresult > thisDie.NumSides - thisDie.DieParameter)
                    { // roll went high-open
                        do
                        {
                            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
                            dieresult += thisroll;
                            result_text += " + " + thisroll.ToString();
                        } while (thisroll > thisDie.NumSides - thisDie.DieParameter);
                    }
                    else if (dieresult <= thisDie.DieParameter)
                    { // roll went low-open
                        do
                        {
                            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
                            dieresult -= thisroll;
                            result_text += " - " + thisroll.ToString();
                        } while (thisroll > thisDie.NumSides - thisDie.DieParameter);
                    }
                    results.Add(DiceResults_Assemble(dieresult, result_text));
                    break;
                case 6: // Open High
                    dieresult += thisroll + thisDie.DieAdder;
                    result_text = dieresult.ToString();
                    if (dieresult > thisDie.NumSides - thisDie.DieParameter)
                    { // roll went high-open
                        do
                        {
                            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
                            dieresult += thisroll;
                            result_text += " + " + thisroll.ToString();
                        } while (thisroll > thisDie.NumSides - thisDie.DieParameter);
                    }
                    results.Add(DiceResults_Assemble(dieresult, result_text));
                    break;
                case 7: // Open Low
                    dieresult += thisroll + thisDie.DieAdder;
                    result_text = dieresult.ToString();
                    if (dieresult <= thisDie.DieParameter)
                    { // roll went low-open
                        do
                        {
                            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
                            dieresult -= thisroll;
                            result_text += " - " + thisroll.ToString();
                        } while (thisroll > thisDie.NumSides - thisDie.DieParameter);
                    }
                    results.Add(DiceResults_Assemble(dieresult, result_text));
                    break;
                case 8: // Count Under
                    if (thisroll + thisDie.DieAdder < thisDie.DieParameter) dieresult++;
                    results.Add(DiceResults_Assemble(thisroll + thisDie.DieAdder, (thisroll + thisDie.DieAdder).ToString()));
                    break;
                case 9: // Count Over
                    if (thisroll + thisDie.DieAdder > thisDie.DieParameter) dieresult++;
                    results.Add(DiceResults_Assemble(thisroll + thisDie.DieAdder, (thisroll + thisDie.DieAdder).ToString()));
                    break;
                case 10: // Total Under
                    if (thisroll < thisDie.DieParameter) dieresult += thisroll + thisDie.DieAdder;
                    results.Add(DiceResults_Assemble(thisroll + thisDie.DieAdder, (thisroll + thisDie.DieAdder).ToString()));
                    break;
                case 11: // Total Over
                    if (thisroll > thisDie.DieParameter) dieresult += thisroll + thisDie.DieAdder;
                    results.Add(DiceResults_Assemble(thisroll + thisDie.DieAdder, (thisroll + thisDie.DieAdder).ToString()));
                    break;
            }
        }
        dieresult = dieresult * thisDie.TotalMultiplier + thisDie.TotalAdder;
        Result.Text = dieresult.ToString();
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

    // Total Highest and Lowest rolls
    public void DiceRoll_TotalSorted(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int dieresult = 0, i, thisroll;
        Random r = new Random();

        // variables used for the sortable list of rolls and results
        List<string> results = new List<string>();
        List<int> rolls = new List<int>();

        // do the rolls and store them 
        for (i = 0; i < qty; i++)
        {
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1 + thisDie.DieAdder);
            rolls.Add(thisroll);
            results.Add(DiceResults_Assemble(thisroll, thisroll.ToString()));
        }

        // sort so you can peel off the right number of them
        if (thisDie.DieType == 2) // Total Highest
        { // sort descending
            rolls.Sort(new Comparison<int>((s1, s2) => s2.CompareTo(s1)));
        }
        else
        { // sort ascending
            rolls.Sort();
        }

        // add up just that many rolls
        for (i = 0; i < thisDie.DieParameter; i++)
        {
            dieresult += rolls[i];
        }

        dieresult = dieresult * thisDie.TotalMultiplier + thisDie.TotalAdder;
        Result.Text = dieresult.ToString();
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

    // Each die is a digit via string concatenation, as used in In Nomine.
    public void DiceRoll_DigitDice(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int i, thisroll;
        Random r = new Random();
        List<string> results = new List<string>();

        Result.Text = "";
        ResultRolls.Text = "";
        for (i = 0; i < qty; i++)
        {
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1 + thisDie.DieAdder);
            results.Add(DiceResults_Assemble(thisroll, thisroll.ToString()));
            Result.Text += thisroll.ToString();
        }
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

    // Dice where it keeps rolling until you fail a check, and report the counts
    public void DiceRoll_SeriesCount(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int dieresult = 0, i, thisroll;
        string resultstext;
        Random r = new Random();
        List<string> results = new List<string>();

        ResultRolls.Text = "";
        for (i = 0; i < qty; i++)
        {
            dieresult = 0;
            resultstext = "";
            while ((thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1 + thisDie.DieAdder)) <= thisDie.DieParameter)
            {
                resultstext += thisroll.ToString() + " ";
                dieresult++;
            }
            resultstext += thisroll.ToString();
            results.Add(DiceResults_Assemble(dieresult, resultstext));
        }
        dieresult = dieresult * thisDie.TotalMultiplier + thisDie.TotalAdder;
        Result.Text = dieresult.ToString();
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

    // Averaging dice are dice where some extreme values are replaced with central values
    public void DiceRoll_Averaging(int qty, Die thisDie, Label Result, Label ResultRolls, Cache theCache)
    {
        int[] AveragingDistribution;
        int i;

        AveragingDistribution = theCache["Averaging_" + thisDie.NumSides.ToString() + "_" + thisDie.DieParameter.ToString()] as int[];
        if (AveragingDistribution == null)
        {
            AveragingDistribution = new int[thisDie.NumSides];

            int j, keysequence, rollover, leftinsert, rightinsert;

            // start with a simple sequence of all possible sides from 1 to NumSides
            //Debug.Text = "Building base distribution " + thisDie.NumSides.ToString() + " " + thisDie.DieParameter.ToString() + "<br />";
            for (i = 0; i < thisDie.NumSides; i++)
            {
                AveragingDistribution[i] = i + 1;
            }

            // these variables are used to create this key sequence: 1 2  1 2 3  1 2 3 4  1 2 3 4 5  etc.
            keysequence = 0;
            rollover = 2;

            // build the distribution by moving values in towards the center by the keysequence
            for (i = 1; i <= thisDie.DieParameter; i++)
            {
                // go to the next item in the key sequence
                keysequence++;
                if (keysequence > rollover)
                {
                    keysequence = 1;
                    rollover++;
                }
                //Debug.Text += "Pass " + i.ToString() + " of distribution building, key " + keysequence.ToString() + "<br />";

                // find the "center" position
                if (thisDie.NumSides % 2 == 1)
                {
                    // odd number of sides
                    leftinsert = (int)(thisDie.NumSides / 2) + 1;
                    rightinsert = leftinsert;
                }
                else
                {
                    // even number of sides
                    leftinsert = thisDie.NumSides / 2;
                    rightinsert = leftinsert + 1;
                }
                //Debug.Text += "Insert is at " + leftinsert.ToString() + " / " + rightinsert.ToString() + "<br />";

                // move down to the next value
                for (j = 1; j <= keysequence - 1; j++)
                {
                    while (leftinsert > 2 && AveragingDistribution[leftinsert] == AveragingDistribution[leftinsert + 1])
                    {
                        leftinsert--;
                    }
                    leftinsert--;
                    while (rightinsert < thisDie.NumSides - 1 && AveragingDistribution[rightinsert] == AveragingDistribution[rightinsert + 1])
                    {
                        rightinsert++;
                    }
                    rightinsert++;
                }
                //Debug.Text += "Moved to next value " + leftinsert.ToString() + " / " + rightinsert.ToString() + "<br />";

                // open a space for the left insert
                for (j = 1; j <= leftinsert - 1; j++)
                {
                    AveragingDistribution[j - 1] = AveragingDistribution[j];
                }
                // the value that used to be there is right, so that's it
                //Debug.Text += "Left insert completed<br />";

                // open a space for the right insert
                for (j = thisDie.NumSides; j >= rightinsert + 1; j--)
                {
                    AveragingDistribution[j - 1] = AveragingDistribution[j - 2];
                }
                // again, the value that used to be there is right
                //Debug.Text += "Right insert completed<br />";

            }
            //Debug.Text += "Built distribution: ";
            //for (i = 0; i < thisDie.NumSides; i++) { Debug.Text += AveragingDistribution[i].ToString() + " "; }
            //Debug.Text += "<br />";

            // save in the cache for later use; since this never changes, we save it for a whole day, but allow it to expire so this won't clog up the cache forever
            theCache.Insert("Averaging_" + thisDie.NumSides.ToString() + "_" + thisDie.DieParameter.ToString(), AveragingDistribution, null, DateTime.Now.AddHours(24), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        // now it's time to roll
        int dieresult = 0, thisroll;
        Random r = new Random();

        // variables used for the sortable list of rolls and results
        List<string> results = new List<string>();

        for (i = 0; i < qty; i++)
        {
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
            thisroll = AveragingDistribution[thisroll - 1];
            dieresult += thisroll + thisDie.DieAdder;
            results.Add(DiceResults_Assemble(thisroll, thisroll.ToString()));
        }
        dieresult = dieresult * thisDie.TotalMultiplier + thisDie.TotalAdder;
        Result.Text = dieresult.ToString();
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

    // A special kind of die which counts crits and botches
    public void DiceRoll_ArsMagica(Die thisDie, Label Result, Label ResultRolls)
    {
        int dieresult = 0, i, thisroll;
        Random r = new Random();

        ResultRolls.Text = "";
        dieresult = 0;
        thisroll = ((int)(r.NextDouble() * thisDie.NumSides));
        if (thisroll == 1)
        {
            ResultRolls.Text = "1 ";
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
            i = 2;
            while (thisroll == 1)
            {
                i *= 2;
                ResultRolls.Text += "1 ";
                thisroll = ((int)(r.NextDouble() * thisDie.NumSides) + 1);
            }
            ResultRolls.Text += thisroll;
            dieresult = ((i * thisroll) + thisDie.DieAdder) * thisDie.TotalMultiplier + thisDie.TotalAdder;
            Result.Text = dieresult.ToString();
        }
        else if (thisroll == 0 && thisDie.DieParameter > 0)
        {
            ResultRolls.Text = "0 [";
            dieresult = 0;
            for (i = 1; i <= thisDie.DieParameter; i++)
            {
                thisroll = ((int)(r.NextDouble() * thisDie.NumSides));
                ResultRolls.Text += thisroll.ToString() + " ";
                if (thisroll == 0) dieresult++;
            }
            ResultRolls.Text = ResultRolls.Text.Trim() + "]";
            if (dieresult == 0)
            {
                Result.Text = (thisDie.DieAdder * thisDie.TotalMultiplier + thisDie.TotalAdder).ToString();
            }
            else if (dieresult == 1)
            {
                Result.Text = "Botch!";
            }
            else
            {
                Result.Text = "Botch! (" + dieresult + ")";
            }
        }
        else if (thisroll == 0 && thisDie.DieParameter == 0)
        {
            ResultRolls.Text = "10";
            Result.Text = "10";
        }
        else
        {
            ResultRolls.Text = thisroll.ToString();
            Result.Text = ((thisroll + thisDie.DieAdder) * thisDie.TotalMultiplier + thisDie.TotalAdder).ToString();
        }
        ResultRolls.Text = ResultRolls.Text.Trim();
    }

    // Die that resolves both stunning and killing damage
    public void DiceRoll_Champions(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int stundamage = 0, bodydamage = 0, i, thisroll;
        Random r = new Random();
        List<string> results = new List<string>();

        for (i = 0; i < qty; i++)
        {
            thisroll = ((int)(r.NextDouble() * thisDie.NumSides)) + 1 + thisDie.DieAdder;
            results.Add(DiceResults_Assemble(thisroll, thisroll.ToString()));
            if (thisDie.DieParameter == 0)
            {
                // killing attack
                bodydamage += thisroll;
            }
            else
            {
                stundamage += thisroll;
                if (thisroll <= thisDie.DieParameter)
                {
                    // counts as 0 body damage; nothing happens
                }
                else if (thisroll >= thisDie.NumSides - thisDie.DieParameter + 1)
                {
                    // counts as 2
                    bodydamage += 2;
                }
                else
                {
                    // counts as 1
                    bodydamage++;
                }
            }
        }
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
        if (thisDie.DieParameter == 0)
        { // calculate stun die multiplier
            thisroll = (int)(r.NextDouble() * 6);
            stundamage *= thisroll;
            ResultRolls.Text += " [stun mult " + thisroll + "]";
        }
        Result.Text = bodydamage.ToString() + "b, " + stundamage.ToString() + "s";
    }

    // Each die is a digit via string concatenation, as used in In Nomine.
    public void DiceRoll_DividedDice(int qty, Die thisDie, Label Result, Label ResultRolls)
    {
        int i, numerator, denominator, thisroll, dieresult = 0;
        Random r = new Random();
        List<string> results = new List<string>();

        Result.Text = "";
        ResultRolls.Text = "";
        for (i = 0; i < qty; i++)
        {
            numerator = ((int)(r.NextDouble() * thisDie.NumSides) + 1 + thisDie.DieAdder);
            denominator = ((int)(r.NextDouble() * thisDie.DieParameter) + 1 + thisDie.DieAdder);
            thisroll = (int)(((float)numerator / (float)denominator) + 0.5);
            dieresult += thisroll * thisDie.TotalMultiplier + thisDie.TotalAdder;
            results.Add(DiceResults_Assemble(thisroll, numerator.ToString() + "/" + denominator.ToString()));
        }
        Result.Text += dieresult.ToString();
        BuildResultRolls(thisDie.SortRolls, results, ResultRolls);
    }

}

