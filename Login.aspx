﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/MasterPages/Frontend.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMainContent" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" BorderStyle="Inset" BorderWidth="3px">
        <div align="center">
            <asp:LoginView ID="LoginView1" runat="server">
            <AnonymousTemplate>
                <asp:Login ID="Login1" runat="server" CreateUserText="Create an account" CreateUserUrl="~/CreateAccount.aspx" TitleText="" PasswordRecoveryText="Reset your password" PasswordRecoveryUrl="~/ForgotPassword.aspx" RememberMeSet="True" RememberMeText="Remember me" UserNameLabelText="Username:" UserNameRequiredErrorMessage="Username is required.">
                </asp:Login>
            </AnonymousTemplate>
            <LoggedInTemplate>
                <asp:LoginName ID="LoginName1" runat="server" FormatString="You're already logged in, {0}." /><br />
                Maybe you'd prefer to <asp:LoginStatus ID="LoginStatus1" runat="server" LoginText="Log In" LogoutText="log out" /> or <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ChangePassword.aspx">change your password</asp:HyperLink>?
            </LoggedInTemplate>
        </asp:LoginView>
        </div>
        <br />
        <div align="center"><asp:Button ID="Button1" runat="server" Text="Cancel" OnClick="Button1_Click" /></div>
        </asp:Panel>
</asp:Content>

