# PrismDice #

This is an ASP.NET application written in C# and configured to be posted to a hosting site like Azure, or hosted locally using SQL Server (any edition) and IIS.

### The application ###

By default the program presents a feature-rich dice roller and calculator. You can roll any number of types of dice and combine the rolls with a calculator, either by using the calculator to enter or calculate the number of dice to roll, or by performing operations on the calculations. The calculator includes memories and other functions that might make sense to combine with dice rolls, but not scientific functions that don't.

If you create a user account, you then become able to configure multiple dice sets, where you can specify any kind of dice to roll, including special types used in various roleplaying games such as open-ended and dice pools. Each set of dice contains eight dice buttons which can be labelled and configured as you wish. You can also publish your configured dice so that other users can copy them into their own sets, and of course, that means you can copy dice configurations other users have set up.

### Purpose ###

This was written as I was teaching myself ASP.NET 4.5 as a means to be sure I knew what I was learning and as a demo. I hosted it on a free Azure account for a couple of months, but right now I don't have anywhere to host it.