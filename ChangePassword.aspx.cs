﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _ChangePassword : BasePage
{
    MembershipUser thisUser;

    protected void Page_Load(object sender, EventArgs e)
    {

        thisUser = Membership.GetUser(User.Identity.Name);

        // If there's a logged-in user, preload their email address into the text box
        if (!Page.IsPostBack && (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
        {
            TextBox tbox = (TextBox)(LoginView1.FindControl("EmailAddress"));
            tbox.Text = thisUser.Email;
        }
    }

    protected void ChangeEmail_Click(object sender, EventArgs e)
    {
        // Save the changed user email address
        TextBox tbox = (TextBox)(LoginView1.FindControl("EmailAddress"));
        Label msg = (Label)(LoginView1.FindControl("EmailChangeMessage"));

        try
        {
            thisUser.Email = tbox.Text;
            Membership.UpdateUser(thisUser);
            msg.Text = "Your email has been updated.";
        }
        catch (System.Configuration.Provider.ProviderException err)
        {
            msg.Text = err.Message;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }

}

